# BibtexRetrieval
Shell script to retrive Bibtex information from ottobib.com using the ISBN.

This is is a simple script that retrives a Bibtex entry from ottobib.com for the provided ISBN.

To use just type isbn2bibtex.sh followed by the book's ISBN.

Before using for the first time edit line 5 of the script to save bibtex information to the desired file.

Currently it is set to save to ~/Desktop/isbn.bib

Also if the file does not exist create an empty file using touch.

displays
